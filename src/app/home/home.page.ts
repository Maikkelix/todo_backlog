import { Component, OnInit } from '@angular/core';
import { Task } from "../../task";
import { TasksService } from "../tasks.service";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  inprogress = Array<Task>();
  todo = Array<Task>();
  finished = Array<Task>();

  constructor(public tasksService: TasksService) {}

  ngOnInit() {
    this.inprogress = this.tasksService.inprogress;
    this.todo = this.tasksService.todo;
    this.finished = this.tasksService.finished;
  }

  doReorder(ev: any) {
    console.log(ev);
    ev.detail.complete();
  }

}
